Ext.define('PaidSearch.view.Viewport', {
    extend: 'Ext.container.Viewport',
    id: 'Viewport',
    layout: 'hbox',
    height: "100%",
    width: "100%",

    initComponent: function() {
        var me = this;

        var leftPanel = Ext.create('PaidSearch.view.panel.mainMenu',{
            hidden: true
        });

        var navPanel = Ext.create('PaidSearch.view.panel.navPanel',{
            padding: 15,
            height: '100%',
            width: '100%'
        });

        var rightPanel = Ext.create('PaidSearch.view.panel.mainPanel', {
            height: '100%',
            hidden: true,
            flex: 1,
            shadow: true,
            items:[
                navPanel
            ]
        });

        var loginForm = Ext.create('PaidSearch.view.form.loginForm');

        var loginPanel = Ext.create('PaidSearch.view.panel.loginPanel',{
           items:[loginForm]
        });

        me.items = [leftPanel, rightPanel, loginPanel]



        me.callParent(arguments);
    }
});
