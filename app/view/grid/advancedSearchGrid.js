Ext.define('PaidSearch.view.grid.advancedSearchGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'advancedSearchGrid',
    id: 'advancedSearchGrid',
    controller: 'advancedSearchController',

    store: {
        fields: [
            {name: 'flagged'},
            {name: 'queue_id'},
            {name: 'job_state'},
            {name: 'requested_dt'},
            {name: 'company_name'},
            {name: 'phrase'},
            {name: 'engine_name'},
            {name: 'client_tag'},
            {name: 'reviewed'}
        ],
        autoLoad: false,
        proxy: {
            type: 'direct',
            directFn: "reporting.getAdvancedSearchGrid"
        }

    },
    tbar: [
        '->',
        // {
        //     xtype: "textfield",
        //     name: "searchPhrase",
        //     emptyText: "Search Phrase"
        // },{
        //             xtype: "multiselector",
        //             emptyText: "SearchEngine",
        //             name: "searchEngine",
        //             valueField: "engine_id",
        //             displayField: "engine_name",
        //             queryMode: "local",
        //             width: 500,
        //             store: {
        //                 fields: [
        //                     {name: "engine_name", type: "string"},
        //                     {name: "engine_id", type: "int"}
        //                 ],
        //                 autoLoad: true,
        //                 proxy: {
        //                     type: 'direct',
        //                     directFn: "reporting.getSearchEngines"
        //                 }
        //             }
        // },
        // {
        //     xtype: "datefield",
        //     name: "fromDate",
        //     emptyText: 'From Date'
        //
        // },
        // {
        //     xtype: "datefield",
        //     name: "toDate",
        //     emptyText: 'To Date'
        //
        // },
        {
            xtype: "button",
            text: "Run Search",
            iconCls: 'x-fa fa-file-alt',
            tooltip: "Run Report",
            action: "openSearchForm"
        }

    ],

    columns:[
        {
            dataIndex: 'flagged',
            text: 'Flagged',
            flex: 1,
            height: 40
        },{
            dataIndex: 'queue_id',
            text: 'Job ID',
            flex: 1,
            height: 40
        },{
            dataIndex: 'job_state',
            text: 'Status',
            flex: 1,
            height: 40
        },{
            dataIndex: 'requested_dt',
            text: 'Created Date',
            flex: 1,
            height: 40
        },{
            dataIndex: 'company_name',
            text: 'Company',
            flex: 1,
            height: 40
        },{
            dataIndex: 'phrase',
            text: 'Phrase',
            flex: 1,
            height: 40
        },{
            dataIndex: 'engine_name',
            text: 'Search Engine',
            flex: 1,
            height: 40
        },{
            dataIndex: 'client_tag',
            text: 'Tag',
            flex: 1,
            height: 40
        },{
            dataIndex: 'reviewed',
            text: 'Reviewed',
            flex: 1,
            height: 40
        },

    ]
});
