Ext.define('PaidSearch.view.grid.recentSearchGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'recentSearchGrid',
    id: 'recentSearchGrid',
    controller: 'recentSearchController',
    // cls: 'screenshotCls',
    store: {
        fields: [
            {name: 'flagged'},
            {name: 'queue_id'},
            {name: 'job_state'},
            {name: 'requested_dt'},
            {name: 'company_name'},
            {name: 'phrase'},
            {name: 'engine_name'},
            {name: 'client_tag'},
            {name: 'reviewed'}
        ],
        autoLoad: false,
        proxy: {
            type: 'direct',
            directFn: "reporting.getRecentSearches"
        }

    },
    items: [
    //     {
    //     xtype: 'toolbar',
    //     docked: 'top',
    //     items: [
    //         {
    //             xtype: 'button',
    //             iconCls: 'x-fa fa-list',
    //             handler: 'openMenu'
    //         },
    //         {
    //             xtype: 'button',
    //             text: 'Upload',
    //             handler: 'launchUploadForm'
    //         },'->',
    //         {
    //             xtype: 'datefield',
    //             value: new Date()
    //         },
    //         {
    //             xtype: 'button',
    //             text: 'Refresh',
    //             handler: 'screenshotGridRefresh'
    //         }
    //     ]
    // }
    ],

    columns:[
        {
            dataIndex: 'flagged',
            text: 'Flagged',
            flex: 1,
            height: 40
        },{
            dataIndex: 'queue_id',
            text: 'Job ID',
            flex: 1,
            height: 40
        },{
            dataIndex: 'job_state',
            text: 'Status',
            flex: 1,
            height: 40
        },{
            dataIndex: 'requested_dt',
            text: 'Created Date',
            flex: 1,
            height: 40
        },{
            dataIndex: 'company_name',
            text: 'Company',
            flex: 1,
            height: 40
        },{
            dataIndex: 'phrase',
            text: 'Phrase',
            flex: 1,
            height: 40
        },{
            dataIndex: 'engine_name',
            text: 'Search Engine',
            flex: 1,
            height: 40
        },{
            dataIndex: 'client_tag',
            text: 'Tag',
            flex: 1,
            height: 40
        },{
            dataIndex: 'reviewed',
            text: 'Reviewed',
            flex: 1,
            height: 40
        },

    ]
});
