Ext.define('PaidSearch.view.form.advancedSearchForm', {
    extend: 'Ext.form.Panel',
    xtype: 'advancedSearchForm',
    layout: 'form',

    controller: 'advancedSearchController',
    api: {
        submit: 'reporting.pullAdvancedSearch'
    },

    items: [
        {
            xtype: 'textfield',
            name: 'searchPhrase',
            emptyText: 'Search Phrase',
        },
        {
            xtype: "datefield",
            name: "fromDate",
            emptyText: 'From Date'
        },
        {
            xtype: "datefield",
            name: "toDate",
            emptyText: 'To Date'
        },

        {
            xtype: "tagfield",
            emptyText: "SearchEngine",
            name: "searchEngine",
            valueField: "engine_id",
            displayField: "engine_name",
            queryMode: "local",
            publishes: 'value',
            createNewOnEnter: true,
            createNewOnBlur: true,
            filterPickList: true,
            store: {
                fields: [
                    {name: "engine_name", type: "string"},
                    {name: "engine_id", type: "int"}
                ],
                autoLoad: true,
                proxy: {
                    type: 'direct',
                    directFn: "reporting.getSearchEngines"
                }
            }
        },

        {
            xtype: 'button',
            text: 'Submit',
            action: 'submitAdvancedSearch'
        }

    ],
})
