Ext.define('PaidSearch.view.form.loginForm', {
    extend: 'Ext.form.Panel',
    xtype: 'loginForm',
    frame: false,
    width: 550,
    bodyPadding: 10,
    height: 500,
    baseCls: 'loginPanel',
    controller: 'loginController',
    api: {
        submit: 'authentication.userLogin'
    },

    items: [{
            xtype: 'image',
            height: 126,
            width: 525,
            margin: '0 0 0 0',
            alt: 'integrishield-logo',
            src: 'resources/images/is_logo_yellow.png'
        },
        {
            xtype: 'textfield',
            allowBlank: false,
            name: 'email',
            emptyText: 'email',
            inputType: 'email',
            fieldCls: 'loginTextFields',
        }, {
            xtype: 'textfield',
            allowBlank: false,
            name: 'password',
            emptyText: 'password',
            inputType: 'password',
            fieldCls: 'loginTextFields',
        }, {
            xtype: 'button',
            text: 'Login',
            cls: 'loginBtn',
            action: 'submitLogin'
        }],

    defaults: {
        anchor: '100%',
        labelWidth: 120
    }
})
