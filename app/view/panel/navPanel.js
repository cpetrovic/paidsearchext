Ext.define('PaidSearch.view.panel.navPanel', {
    extend: 'Ext.tab.Panel',
    xtype: 'navPanel',
    layout: 'fit',
    controller: 'navController',

    requires: [
        'Ext.layout.Fit'
    ],

    initComponent: function() {
        let me = this;

        let rs = Ext.widget('recentSearchGrid',{
            title: 'Recent Searches',
            layout: 'fit'
        });

        let fs = Ext.widget('flaggedSearchGrid',{
            title: 'Flagged Searches',
            layout: 'fit'
        });

        let ar = Ext.widget('advancedSearchGrid',{
            title: 'Advanced Reporting',
            layout: 'fit'
        });

        let cns = Ext.widget('plainPanel',{
            title: 'Create New Search',
            layout: 'fit'
        });

        me.items = [rs, fs, ar, cns];

        me.callParent(arguments);
    }
});
