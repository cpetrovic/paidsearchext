Ext.define('PaidSearch.view.panel.mainPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'mainPanel',
    layout: 'fit',

    initComponent: function() {
        let me = this;

        me.callParent(arguments);
    }
});
