Ext.define('PaidSearch.view.panel.loginPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'loginPanel',
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },
    height: '100%',
    width: '100%',
    bodyStyle:{"background-color":"#125d72"},
    constructor: function() {
        let me = this;
        me.callParent(arguments);
    }
});
