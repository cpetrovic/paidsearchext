Ext.define('PaidSearch.view.panel.plainPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'plainPanel',
    layout: 'fit',

    constructor: function() {
        var me = this;
        me.callParent(arguments);
    }
});
