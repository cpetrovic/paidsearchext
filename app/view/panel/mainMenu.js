Ext.define('PaidSearch.view.panel.mainMenu', {
    extend: 'Ext.panel.Panel',
    xtype: 'mainMenu',
    layout: 'fit',
    height: '100%',
    width: 220,
    bodyStyle:{"background-color":"#125D72"},
    controller: 'mainMenuController',

    items: [
        {
            xtype: 'image',
            src: 'resources/images/is_logo_blue.png',
            alt: 'Integrishield logo',
            maxHeight: 46,
            maxWidth: 190,
            margin: '15px'
        },
        {
            xtype: 'button',
            text: 'PaidSearch',
            iconCls: 'x-fa fa-search-dollar',
            maxHeight: 75,
            margin: '50px 0px 0px 0px',
            action: 'launchPaidSearch'
        }
    ],

    constructor: function() {
        let me = this;
        me.callParent(arguments);
    }
});
