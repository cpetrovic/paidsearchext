Ext.define('PaidSearch.Application', {
    extend: 'Ext.app.Application',

    views: [],

    name: 'PaidSearch',

    launch: function () {
        Ext.direct.Manager.addProvider(Ext.REMOTING_API);
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
