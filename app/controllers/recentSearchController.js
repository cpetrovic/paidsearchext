Ext.define('PaidSearch.controllers.recentSearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.recentSearchController',

    init: function() {
        this.control({
            "recentSearchGrid": {
                rowdblclick: this.openRecordModel
            }
        });
    },

    openRecordModel: function (row, record, element, rowIndex){
        console.log(record)

            let rsg = Ext.getCmp('recordModelWindow');

            if( Ext.isDefined(rsg) ) {
                rsg.show();
            }else{
                asw = Ext.create('Ext.window.Window', {
                    id: 'recordModelWindow',
                    title: '',
                    layout: 'fit',
                    autoShow: true,
                    maximizable: true,
                    resizable: true,
                    modal: true,
                    draggable: true,
                    closable: true,
                    constrain: true,
                    collapsible: true,
                    width: 500,
                    height: 500,
                    items: []
                });
            }
        }
});
