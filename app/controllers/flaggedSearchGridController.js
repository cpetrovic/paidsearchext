Ext.define('PaidSearch.controllers.flaggedSearchGridController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.flaggedSearchGridController',

    init: function() {
        this.control({
            "flaggedSearchGrid": {
                rowdblclick: this.showMsg
            }
        });
    },

    showMsg: function (){
        console.log("double clicked FlaggedSearchGrid row")
    }

});
