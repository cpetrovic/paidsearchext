Ext.define('PaidSearch.controllers.navController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.navController',

    init: function() {
        this.control({
            'navPanel': {
                tabchange: this.navPanelTabChange
            }
        });
    },

    navPanelTabChange: function(tabPanel, newCard, oldCard, opts) {

        if(newCard.title === 'Flagged Searches') {
            console.log('load flagged search grid');
            let gridStore = tabPanel.down('flaggedSearchGrid').getStore()

            gridStore.load({params: {company_id: sessionStorage.company_id}})


        }else if(newCard.title === 'Create New Search'){
            console.log('load Create New Search');

            let cns = Ext.getCmp('createNewRecordWindow');

            let cnw = Ext.create('Ext.window.Window', {
                id: 'createNewRecordWindow',
                title: '',
                layout: 'form',
                autoShow: true,
                maximizable: true,
                resizable: true,
                modal: true,
                draggable: true,
                closable: true,
                constrain: true,
                collapsible: true,
                width: 500,
                height: 500,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'searchPhrase',
                        emptyText: 'Search Phrase',
                    },
                    {
                        xtype: "datefield",
                        name: "fromDate",
                        emptyText: 'From Date'
                    },
                    {
                        xtype: "datefield",
                        name: "toDate",
                        emptyText: 'To Date'
                    },

                    {
                        xtype: "tagfield",
                        emptyText: "SearchEngine",
                        name: "searchEngine",
                        valueField: "engine_id",
                        displayField: "engine_name",
                        queryMode: "local",
                        publishes: 'value',
                        createNewOnEnter: true,
                        createNewOnBlur: true,
                        filterPickList: true,
                        store: {
                            fields: [
                                {name: "engine_name", type: "string"},
                                {name: "engine_id", type: "int"}
                            ],
                            autoLoad: true,
                            proxy: {
                                type: 'direct',
                                directFn: "reporting.getSearchEngines"
                            }
                        }
                    },

                    {
                        xtype: 'button',
                        text: 'Submit',
                        action: 'submitAdvancedSearch'
                    }
                ]
            })

        }
    }


});
