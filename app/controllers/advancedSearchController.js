Ext.define('PaidSearch.controllers.advancedSearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.advancedSearchController',

    init: function() {
        this.control({
            'button[action=openSearchForm]': {
                click: this.openSearchForm
            },
            'button[action=submitAdvancedSearch]': {
                click: this.submitAdvancedSearch
            }
        });
    },

    openSearchForm: function(button) {

        let asw = Ext.getCmp('advancedSearchWindow');

        let asf = Ext.create("PaidSearch.view.form.advancedSearchForm")

        if( Ext.isDefined(asw) ) {
            asw.show();
        }else{
            asw = Ext.create('Ext.window.Window', {
                id: 'advancedSearchWindow',
                title: '',
                layout: 'fit',
                autoShow: true,
                maximizable: true,
                resizable: true,
                modal: true,
                draggable: true,
                closable: true,
                constrain: true,
                collapsible: true,
                width: 500,
                height: 500,
                items: [asf]
            });
        }
    },
    submitAdvancedSearch: function (button){
        console.log("pressed submit for advanced")
        let form = button.up('form')
        form.submit({
            success: function (returnForm, action){

            },
            failure: function (returnForm, action){

            }
        })
    }
});
