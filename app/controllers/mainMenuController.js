Ext.define('PaidSearch.controllers.mainMenuController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.mainMenuController',

    init: function() {
        this.control({
            'button[action=launchPaidSearch]': {
                click: this.paidSearchButtonClick
            }
        });
    },

    paidSearchButtonClick: function(button) {
        
        console.log('paid search button clicked');

    }
});
