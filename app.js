Ext.application({
    extend: 'PaidSearch.Application',

    requires: [
        'PaidSearch.*',
        'Ext.grid.*',
        'Ext.data.*',
        'Ext.panel.*',
        'Ext.direct.*',
        'Ext.form.*'
    ],

    name: 'PaidSearch',
    mainView: 'PaidSearch.view.Viewport'

});
