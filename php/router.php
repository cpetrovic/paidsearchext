<?php

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }


    require('config.php');

    class BogusAction {
        public $action;
        public $method;
        public $data;
        public $tid;
    }

    $isForm = false;
    $isUpload = false;

    $HTTP_RAW_POST_DATA = file_get_contents( 'php://input' );

    if (isset($HTTP_RAW_POST_DATA) && !isset($_REQUEST['extAction'])) {
        header('Content-Type: application/javascript');
        $data = json_decode($HTTP_RAW_POST_DATA);
    }
    else if(isset($_REQUEST['extAction'])){
        $isForm = true;
        if (isset($_REQUEST['extUpload'])){
            $isUpload = $_REQUEST['extUpload'] == 'true';
        }
        $data = new BogusAction();
        $data->action = $_REQUEST['extAction'];
        $data->method = $_REQUEST['extMethod'];
        $data->tid = isset($_REQUEST['extTID']) ? $_REQUEST['extTID'] : null;
        $data->data = array($_REQUEST, $_FILES);

    }
    else {
        die('Invalid request.');
    }

    function doRpc($cdata){
        $API = get_extdirect_api('router');

        try {
            if (!isset($API[$cdata->action])) {
                throw new Exception('Call to undefined action: ' . $cdata->action);
            }

            $action = $cdata->action;
            $a = $API[$action];

            $method = $cdata->method;
            $mdef = $a['methods'][$method];

            if (!$mdef){
                throw new Exception("Call to undefined method: $method " ."in action $action");
            }

            $r = array(
                'type'=>'rpc',
                'tid'=>$cdata->tid,
                'action'=>$action,
                'method'=>$method
            );

            require_once("classes/$action.php");
            $o = new $action();

            if (isset($mdef['len'])) {
                $params = isset($cdata->data) && is_array($cdata->data) ? $cdata->data : array();
            }
            else {
                $params = array($cdata->data);
            }

            if (isset($cdata->metadata)){
                array_push($params, $cdata->metadata);
            }

            $r['result'] = call_user_func_array(array($o, $method), $params);
        }

        catch(Exception $e){
            $r['type'] = 'exception';
            $r['message'] = $e->getMessage();
            $r['where'] = $e->getTraceAsString();
        }

        return $r;
    }

    $response = null;

    if (is_array($data)) {
        $response = array();
        foreach($data as $d){
            $response[] = doRpc($d);
        }
    }
    else{
        $response = doRpc($data);
    }

    if ($isForm && $isUpload){
        echo '<html><body><textarea>';
        echo json_encode($response);
        echo '</textarea></body></html>';
    }
    else{
        echo json_encode($response);
    }
