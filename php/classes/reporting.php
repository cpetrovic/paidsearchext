<?php

class reporting {

    public function __construct() {
        require_once("DataService.php");
        $this->db = DataService::getConnection('integris');
    }

    public function getRecentSearches($params)
    {
        $sql = "SELECT 
            jq.queue_id,
            engine_name,
            phr.phrase,
            job_state,
            jq.requested_dt,
            jq.client_tag,
            jq.reviewed_by,
            company_name,                
            fr.flagged_id AS flagged
            FROM job_queue AS jq
            JOIN ps_engines AS eng ON eng.engine_id = jq.engine_id
            JOIN ps_phrases AS phr ON phr.phrase_id = jq.phrase_id
            JOIN users AS u ON u.user_id = jq.user_id
            JOIN countries AS cnt ON cnt.country_id = eng.country_id
            JOIN languages AS lang ON lang.language_id = eng.language_id
            JOIN companies AS c ON c.company_id = phr.company_id                
            left outer join flagged_results AS fr ON fr.job_queue_id = jq.queue_id
            where phr.company_id = $params->company_id
            and jq.requested_dt >= NOW() - INTERVAL 1 WEEK";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function getFlaggedSearchGrid($params)
    {
        $sql = "SELECT 
            jq.queue_id,
            engine_name,
            phr.phrase,
            job_state,
            jq.requested_dt,
            jq.client_tag,
            jq.reviewed_by,
            company_name,                
            fr.flagged_id AS flagged
            FROM job_queue AS jq
            JOIN ps_engines AS eng ON eng.engine_id = jq.engine_id
            JOIN ps_phrases AS phr ON phr.phrase_id = jq.phrase_id
            JOIN users AS u ON u.user_id = jq.user_id
            JOIN countries AS cnt ON cnt.country_id = eng.country_id
            JOIN languages AS lang ON lang.language_id = eng.language_id
            JOIN companies AS c ON c.company_id = phr.company_id                
            JOIN flagged_results AS fr ON fr.job_queue_id = jq.queue_id
            where phr.company_id = $params->company_id";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function getAdvancedSearchGrid($params)
    {

    }
    public function getSearchEngines($params)
    {
        $sql = "
        select engine_id, engine_name
        from ps_engines";

        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    public function pullAdvancedSearch($params)
    {
        print_r($params);
    }

}


