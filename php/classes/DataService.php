<?php

class DataService
{
    static $instance = false;
    private $cache = array();

    function construct()
    {
        $DataService = self::getInstance();
        return $DataService;
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new DataService();
        }
        return self::$instance;
    }

    public static function getConnection($server)
    {
        require("dbconfig.php");

        $DataService = self::getInstance();

        if (isset($DataService->cache[$server])) {
            return $DataService->cache[$server];
        } else {
            $database_host = null;
            $database_port = null;
            $database_name = null;
            $database_user = null;
            $database_password = null;
            $database_type = null;

            foreach ($$server as $key => $value) {
                $$key = $value;
            }


            try {
                $db = new PDO("mysql:host={$database_host};port={$database_port};dbname={$database_name};charset=utf8", "{$database_user}", "{$database_password}");
            } catch (PDOException $e) {
                $msg = 'Connection failed: ' . $e->getMessage();
            }

            $DataService->cache[$server] = $db;

            return $db;
        }
    }
}
