<?php

    function get_extdirect_api() {

        $API = array(
            'authentication' => array(
                'methods' => array(
                    'userLogin' => array(
                        'len' => 1,
                        'formHandler' => true
                    )
                )
            ),
            'upload' => array(
                'methods' => array(
                    'pullRecentFiles' => array(
                        'len' => 1
                    ),
                    'screenshotUpload' => array(
                        'len' => 1,
                        'formHandler' => true
                    ),
                    'getClients' => array(
                        'len' => 1
                    )
                )
            ),
            'reporting' => array(
                'methods' => array(
                    'getRecentSearches' => array(
                        'len' => 1
                    ),
                    'getFlaggedSearchGrid' => array(
                        'len' => 1
                    ),
                    'getAdvancedSearchGrid' => array(
                        'len' => 1
                    ),
                    'getSearchEngines' => array(
                        'len' => 1
                    ),
                    'pullAdvancedSearch' => array(
                        'len' => 1,
                        'formHandler' => true
                    )
                )
            )
        );

        return $API;
    }
